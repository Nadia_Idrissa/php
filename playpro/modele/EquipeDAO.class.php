<?php

    require_once('./modele/DAO.class.php');
    require_once('./modele/classes/Equipe.class.php');
    require_once('./modele/MembreDAO.class.php');

    /**
     * Classe EquipeDAO.
     * Classe effectuant les opération sur la bd 
     * pour la gestion des Equipes
     *
     * @version 1.0
     * @author Anne-Marie Burns
     * @modified by Kennedy and Younes
     */
    class EquipeDAO extends DAO {

        public static function findByOrderBy($criteres, $tri) {
            $Equipes = array();

            // Fabrication de la clause WHERE
            $clause = "";
            $cles = array_keys($criteres);
            $derniere = end($cles);
            if(count($criteres) > 0) {
                $clause = "WHERE ";
                foreach($criteres as $cle => $contenu) {
                    $clause = $clause . $cle . " = :" . $cle;
                    if ($cle !== $derniere) {
                        $clause = $clause . " AND ";
                    }

                }
            }

            // tri par défaut par date si aucun tri n'est demandé 
            // ou si le paramètre de tri ne correspond à aucun des tris possibles
            if (!is_null($tri)) {
                $tri = " ORDER BY " . $tri . ", date_creation";
            } else {
                $tri = " ORDER BY date_creation";
            }

            $sql = 'SELECT * FROM Partie ' . $clause . $tri;

            try {
                $resultat = self::executerRequete($sql, $criteres);

                if($resultat->rowCount() > 0) {
                    $confs_tab = $resultat->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($confs_tab as $Equipe) {
                        array_push($Equipes, New Equipe($Equipe));
                    }
                } 

            } catch(Exception $e) {
                throw new Exception("La requête n'a pas pu être exécutée");
            }

            return $Equipes;
        }

        /**
         * Lit la liste des Equipes
         * @return List<Equipe> liste des Equipes
         * @throws Exception Si la requête a échouée
         */
        public static function findAll() {
            $Equipes = array();
            $mdao = new MembreDAO();
            $membre = NULL;

            $sql = 'SELECT equipe.nom_equipe, equipe.capitaine,equipe.sport, equipe.nb_parties_jouees,equipe.nb_joueurs, equipe.nb_max_joueurs, equipe.date_creation FROM Equipe';


            try {
                $resultat = self::executerRequete($sql);

                if($resultat->rowCount() > 0) {
                    $confs_tab = $resultat->fetchAll(PDO::FETCH_ASSOC);
                
                    foreach ($confs_tab as $element) {
                        
                        
                        $membre = $mdao->findById($element['capitaine']);
                        


                        $Equipe['nom_equipe'] = $element['nom_equipe'];
                        $Equipe['capitaine'] = $membre;
                        $Equipe['sport'] = $element['sport'];
                        $Equipe['nb_parties_jouees'] = $element['nb_parties_jouees'];
                        $Equipe['nb_joueurs'] = $element['nb_joueurs'];
                        $Equipe['nb_max_joueurs'] = $element['nb_max_joueurs'];
                        $Equipe['date_creation'] = $element['date_creation'];
                        array_push($Equipes, New Equipe($Equipe));
                    }
                } 
                

            } catch(Exception $e) {
                throw new Exception("La requête n'a pas pu être exécutée");
            }
            return $Equipes;
        }

        /**
         * Cherche un Equipe à partir de son titre
         * 
         * @param string $id le id du Equipe
         * @return Equipe objet représentant le Equipe
         * @throws Exception Si la Equipe n'existe pas dans la bd
         */
        public static function findBytitre($nom_equipe) {
            $Equipe = NULL;

            $sql = 'SELECT * FROM Equipe WHERE nom_equipe = :nom_equipe';
            $resultat = self::executerRequete($sql, array('nom_equipe' => $nom_equipe));
            
            if($resultat->rowCount() > 0) {
                
                $Equipe = New Equipe($resultat->fetch(PDO::FETCH_ASSOC));//PDO::FETCH_OBJ);
            } 

            return $Equipe;
        }
        
        /**
         * Ajoute une équipe à la bd
         * 
         * @param Equipe $Equipe objet représentant l'équipe à ajouter
         * @return Equipe $Equipe objet représentant l'équipe qui a été ajoutée
         * L'objet Equipe contient un id s'il a bien été ajouté à la bd
         * @throws Exception Si la Equipe n'a pas pu être créée
         */
        public static function create($Equipe) {
            $membre = NULL;
            $mdao = new MembreDAO();
            
            $sql = 'INSERT INTO Equipe (nom_equipe, capitaine, sport ,nb_parties_jouees,nb_joueurs, nb_max_joueurs, date_creation ) VALUES (:nom_equipe, :capitaine, :sport, :nb_parties_jouees, :nb_joueurs, :nb_max_joueurs, :date_creation )';
            
            try {
                $resultat = self::executerRequete($sql, $Equipe->toArray());

                if($resultat->rowCount() > 0) {
                    $membre = $mdao->findById($Equipe->getCapitaine());

                    $sql = 'UPDATE Membre SET equipe = :new_equipe WHERE id = :id';

                    try{
                        $new_equipe = $Equipe->getNom_equipe();
                        $id = $membre->getID();
                        $resultat = self::executerRequete($sql, array('new_equipe'=>$new_equipe, 'id'=>$id));

                    }catch(Exception $e){
                        throw new Exception("Nous n'avons pas pu vous intégrer à cette équipe", 1);
                        
                    }
                   
                    $Equipe->setCapitaine($membre);
                    

                } else {

                    throw new Exception("L'équipe n'a pas pu du tout être ajoutée à la base de données");
                }

            } catch(Exception $e) {
                throw new Exception("L'équipe n'a pas pu être ajoutée à la base de données");
            }

            return $Equipe;

        }

        public static function deleteByID($id){
            $reussite = false;
            

            $sql = 'UPDATE Membre SET equipe = NULL WHERE equipe = :id';

            try{
                $resultat = self::executerRequete($sql, array('id'=>$id));

            }catch(Exception $e){
                throw new Exception("Nous n'avons pas pu supprimer les joueurs de cette équipe", 1);
                
            }
            $sql = 'DELETE  FROM Equipe WHERE nom_equipe = :id';
            
             try {

                $resultat = self::executerRequete($sql, array('id' => $id));

                if($resultat->rowCount() > 0) {
                    $reussite = true;

                }else{
                    throw new Exception ("L'équipe n'a pas pu être supprimée.");
                }

             } catch(Exception $e){
                throw new Exception ("Une erreur est survenue lors de l'effacement.");
             }

             return $reussite;
        }

        public static function update($Equipe){
            

            // Fabrication de la clause SET
            $reussite = false;
            $clause_set = "";
            $param = $Equipe->toArray();
            $cles = array_keys($param);
            $derniere = end($cles);
            if(count($param) > 0) {
                $clause_set = " SET ";
                foreach($param as $cle => $contenu) {
                    if($cle !=='nom_equipe'){

                        $clause_set = $clause_set . $cle . " = :" . $cle;
                        if ($cle !== $derniere) {
                            $clause_set = $clause_set . ", ";
                        }
                    }

                }
            }
            
            $sql = 'UPDATE Equipe' . $clause_set .' WHERE nom_equipe = :nom_equipe';


            try{
                $resultat = self::executerRequete($sql, $Equipe->toArray());

                if($resultat->rowCount() >0){
                
                    $id = self::getBdd()->lastInsertId();
                    $Equipe->setNom_equipe($id);
                    $reussite = true;

                }else{
                    throw new Exception("Le Equipe numéro " . $Equipe->getNom_equipe() . " n\'a pas été mise à jour.");
                

                }
            }catch(Exception $e){
                throw new Exception("Problème survenu lors de la création de la requête.", 1);
                
            }

            return $reussite;
        }

        public static function integrer($pseudo, $nom_equipe){
            $reussite = false;
            $new_equipe = "";

            $sql = 'UPDATE Membre SET equipe = :nom_equipe WHERE pseudo = :pseudo';

            try{
                $resultat = self::executerRequete($sql, array('nom_equipe'=>$nom_equipe, 'pseudo'=>$pseudo));
                if($resultat->rowCount() >0){
                    $equipe = self::findBytitre($nom_equipe);

                    $nb = $equipe->getNb_joueurs();
                    $nb++;

                    $sql = 'UPDATE Equipe SET nb_joueurs = :nb WHERE nom_equipe = :nom_equipe';

                    try{
                        $resultat = self::executerRequete($sql, array('nb'=>$nb, 'nom_equipe'=>$nom_equipe));

                    }catch(Exception $e){
                        throw new Exception("Nous n'avons pas pu incrémenter  le nombre de joueurs de cette équipe", 1);
                        
                    }
                    $reussite = true;

                }else{
                    throw new Exception("Nous n'avons pas pu vous insérer dans cette équipe.");

                }

            }catch(Exception $e){
                throw new Exception("Nous n'avons pas pu vous insérer dans cette équipe.", 1);
                
            }

            return $reussite;
        }

        public static function quitter($pseudo, $nom_equipe){
            $reussite = false;
            

            $sql = 'UPDATE Membre SET equipe = null WHERE pseudo = :pseudo';

            try{
                $resultat = self::executerRequete($sql, array( 'pseudo'=>$pseudo));
                if($resultat->rowCount() >0){
                    $equipe = self::findBytitre($nom_equipe);

                    $nb = $equipe->getNb_joueurs();
                    $nb--;

                    $sql = 'UPDATE Equipe SET nb_joueurs = :nb WHERE nom_equipe = :nom_equipe';

                    try{
                        $resultat = self::executerRequete($sql, array('nb'=>$nb, 'nom_equipe'=>$nom_equipe));

                    }catch(Exception $e){
                        throw new Exception("Nous n'avons pas pu décrémenter  le nombre de joueurs de cette équipe", 1);
                        
                    }
                    $reussite = true;

                }else{
                    throw new Exception("Nous n'avons pas pu vous insérer dans cette équipe.");
                
                }

            }catch(Exception $e){
                throw new Exception("Nous n'avons pas pu vous insérer dans cette équipe.", 1);
                
            }

            return $reussite;
        }

        public static function findJoueurs($id){
            // echo $id;
            $joueurs = array();
            $mdao = new MembreDAO();
            $membre = NULL;

            $sql = 'SELECT pseudo FROM Membre WHERE equipe =  :id ';

            $params = array('id' => $id  );
            
            try {
                $resultat = self::executerRequete($sql, $params );

            
                $tab = $resultat->fetchAll(PDO::FETCH_ASSOC);
                
            
                foreach ($tab as $element) {
                    
                    $membre = $mdao->findBytitre($element['pseudo']);                      
                    array_push($joueurs, $membre);
                }
                
                

            } catch(Exception $e) {
                throw new Exception("Cette équipe n'a pas de joueur.");
            }
           
            return $joueurs;
        }

    }
