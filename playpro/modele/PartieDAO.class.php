<?php

	require_once('./modele/DAO.class.php');
    require_once('./modele/classes/Partie.class.php');
    require_once('./modele/EquipeDAO.class.php');

    /**
     * Classe PartieDAO.
     * Classe effectuant les opération sur la bd 
     * pour la gestion des Parties
     *
     * @version 1.0
     * @author Anne-Marie Burns
     * @modified by Kennedy and Younes
     */
	class PartieDAO extends DAO {

        public static function findByOrderBy($criteres, $tri) {
       
            $Parties = array();

            $sql = 'SELECT * FROM Partie WHERE sport = :sport AND date_partie_heure >= :date_partie_heure ';
            $params = array ("sport" => $criteres['sport'], 'date_partie_heure' => $criteres['date']);
            if ($criteres['sport'] ==="Tous les sports"){
              $sql = 'SELECT * FROM Partie WHERE  date_partie_heure >= :date_partie_heure ';
              $params = array ( 'date_partie_heure' => $criteres['date']);
            }

            try {
                $resultat = self::executerRequete($sql, $params);
                
                if($resultat->rowCount() > 0) {
                    $tab = $resultat->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($tab as $Partie) {
                        array_push($Parties, New Partie($Partie));
                    }
                } 

            } catch(Exception $e) {
                throw new Exception("La requête n'a pas pu être exécutée");
            }

            return $Parties;
        }

        /**
         * Lit la liste des Parties
         * @return List<Partie> liste des Parties
         * @throws Exception Si la requête a échouée
         */
        public static function findAll() {
            
            $parties = array();
            $edao = new EquipeDAO();
            $equipe = NULL;

             $sql = 'SELECT * FROM Partie';

            try {
                $resultat = self::executerRequete($sql);

                if($resultat->rowCount() > 0) {
                    $parties_tab = $resultat->fetchAll(PDO::FETCH_ASSOC);

                    
                    foreach ($parties_tab as $element) {

                        $partie['id_partie'] = $element['id_partie'];
                        $partie['date_partie_heure'] = $element['date_partie_heure'];
                        $partie['sport'] = $element['sport'];
                        $partie['lieu_partie'] = $element['lieu_partie'];
                        $partie['score'] = $element['score'];
                        $partie['equipe_1'] = $element['equipe_1'];
                        $partie['equipe_2'] = $element['equipe_2'];

                        array_push($parties, New Partie($partie));
                    }
                } 

            } catch(Exception $e) {
                throw new Exception("La requête n'a pas pu être exécutée");
            }
            
            return $parties;
        }

        /**
         * Cherche un Partie à partir de son titre
         * 
         * @param string $id le id du Partie
         * @return Partie objet représentant le Partie
         * @throws Exception Si la Partie n'existe pas dans la bd
         */

   
   public static function findByID($id) {
            $partie = array();

            $sql = 'SELECT * FROM Partie WHERE id_partie = :id_partie';
            $resultat = self::executerRequete($sql, array('id_partie' => $id));

            if($resultat->rowCount() > 0) {
                
                $partie = New Partie($resultat->fetch(PDO::FETCH_ASSOC));//PDO::FETCH_OBJ);
            } 
           

            return $partie;
        }
   
   
        
		public static function findByEquipe($nomEquipe) {
            

            $parties = array();
            $edao = new EquipeDAO();
            $equipe = NULL;

             $sql = 'SELECT * FROM Partie WHERE  equipe_1 = :equipe_1 OR equipe_2 = :equipe_2' ;

            
            try {
                $resultat = self::executerRequete($sql, array("equipe_1" => $nomEquipe, "equipe_2" => $nomEquipe));

                if($resultat->rowCount() > 0) {
                    $parties_tab = $resultat->fetchAll(PDO::FETCH_ASSOC);

                    
                    foreach ($parties_tab as $element) {

                        
                        array_push($parties, New Partie($element));
                    }

                } 

            } catch(Exception $e) {
                throw new Exception("La requête n'a pas pu être exécutée");
          }
          
         
          return $parties;
		}
        
  //       /**
  //        * Ajoute une équipe à la bd
  //        * 
  //        * @param Partie $Partie objet représentant l'équipe à ajouter
  //        * @return Partie $Partie objet représentant l'équipe qui a été ajoutée
  //        * L'objet Partie contient un id s'il a bien été ajouté à la bd
  //        * @throws Exception Si la Partie n'a pas pu être créée
  //        */
		public static function create($Partie) {
      $resultat = false;
        
		    $sql = 'INSERT INTO Partie ( date_partie_heure, sport, equipe_1, equipe_2, lieu_partie, score) VALUES ( :date_partie_heure, :sport, :equipe_1, :equipe_2, :lieu_partie, :score)';

            	$resultat = self::executerRequete($sql, $Partie->toArray());
		    try {
             
            	if($resultat->rowCount() > 0) {

                	$id_partie = self::getBdd()->lastInsertId(); 
                	// récupère l'id de la dernière équipe créée en cas de succès
                    $Partie->setId_partie($id_partie+1);

            	} else {

            		throw new Exception("La partie n'a pas pu être ajoutée à la base de données. Merci");
            	}

            } catch(Exception $e) {
                throw new Exception("La partie n'a pas pu être ajoutée à la base de données");
            }

            return $Partie;

		}

		public static function deleteByID($id_partie){
			$reussite = false;
			 $sql = 'DELETE  FROM Partie WHERE id_partie = :id_partie';
            $resultat = self::executerRequete($sql, array('id_partie' => $id_partie));
			 try {

	            if($resultat->rowCount() > 0) {
	                $reussite = true;

	            }else{
	            	throw new Exception ("La partie n'a pas pu être supprimée.");
	            }

			 } catch(Exception $e){
			 	throw new Exception ("Une erreur est survenue lors de l'effacement.");
			 }

			 return $reussite;
		}

		public static function update($Partie){
			print_r($Partie->toArray());

			$reussite = false;
			$clause_set = "";
			$param = $Partie->toArray();
			$cles = array_keys($param);
			$derniere = end($cles);
			if(count($param) > 0) {
				$clause_set = " SET ";
				foreach($param as $cle => $contenu) {
					if($cle !=='id_partie'){

						$clause_set = $clause_set . $cle . " = :" . $cle;
						if ($cle !== $derniere) {
							$clause_set = $clause_set . ", ";
						}
					}

				}
			}

			$sql = 'UPDATE Partie' . $clause_set .' WHERE id_partie = :id_partie';


			try{
				$resultat = self::executerRequete($sql, $Partie->toArray());

				if($resultat->rowCount() >0){
				
					$id = self::getBdd()->lastInsertId();
					$Partie->setId_partie($id);
					$reussite = true;

				}else{
					throw new Exception("Le Partie numéro " . $Partie->getID() . " n\'a pas été mise à jour.");
				

				}
			}catch(Exception $e){
				throw new Exception("Problème survenu lors de la création de la requête.", 1);
				
			}

			return $reussite;
		}

	}