<?php

    require_once('./modele/DAO.class.php');
    require_once('./modele/classes/Membre.class.php');
    require_once('./modele/EquipeDAO.class.php');

    /**
     * Classe MembreDAO.
     * Classe effectuant les opération sur la bd 
     * pour la gestion des membres
     *
     * @version 1.0
     * @author Anne-Marie Burns
     * @modified by Kennedy and Younes
     */
    class MembreDAO extends DAO {

        public static function findByOrderBy($criteres, $tri) {
            $Membres = array();

            // Fabrication de la clause WHERE
            $clause = "";
            $cles = array_keys($criteres);
            $derniere = end($cles);
            if(count($criteres) > 0) {
                $clause = "WHERE ";
                foreach($criteres as $cle => $contenu) {
                    $clause = $clause . $cle . " = :" . $cle;
                    if ($cle !== $derniere) {
                        $clause = $clause . " AND ";
                    }

                }
            }

            // tri par défaut par date si aucun tri n'est demandé 
            // ou si le paramètre de tri ne correspond à aucun des tris possibles
            if (!is_null($tri)) {
                $tri = " ORDER BY " . $tri . ", date_inscription";
            } else {
                $tri = " ORDER BY date_inscription";
            }

            $sql = 'SELECT * FROM Membre ' . $clause . $tri;

            try {
                $resultat = self::executerRequete($sql, $criteres);

                if($resultat->rowCount() > 0) {
                    $confs_tab = $resultat->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($confs_tab as $Membre) {
                        array_push($Membres, New Membre($Membre));
                    }
                } 

            } catch(Exception $e) {
                throw new Exception("La requête n'a pas pu être exécutée");
            }

            return $Membres;
        }

        /**
         * Lit la liste des membres
         * @return List<Membre> liste des membres
         * @throws Exception Si la requête a échouée
         */
        public static function findAll() {
            $Membres = array();

            $sql = 'SELECT * FROM Membre';

            try {
                $resultat = self::executerRequete($sql);

                if($resultat->rowCount() > 0) {
                    $confs_tab = $resultat->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($confs_tab as $Membre) {
                        array_push($Membres, New Membre($Membre));
                    }
                } 

            } catch(Exception $e) {
                throw new Exception("La requête n'a pas pu être exécutée");
            }
            return $Membres;
        }

        /**
         * Cherche un membre à partir de son titre
         * 
         * @param string $id le id du membre
         * @return Membre objet représentant le membre
         * @throws Exception Si la membre n'existe pas dans la bd
         */
        public static function findById($id) {
            $Membre = NULL;
            

            $sql = 'SELECT * FROM membre WHERE id = :id';
            $resultat = self::executerRequete($sql, array('id' => $id));

            
            if($resultat->rowCount() > 0) {
                $memb = $resultat->fetch(PDO::FETCH_ASSOC);
                $Membre = New Membre($memb);//PDO::FETCH_OBJ);

            } 

            return $Membre;
        }

        public static function findBytitre($id) {
            $Membre = NULL;
            

            $sql = 'SELECT * FROM membre WHERE pseudo = :id';
            $resultat = self::executerRequete($sql, array('id' => $id));

            if($resultat->rowCount() > 0) {
                $memb = $resultat->fetch(PDO::FETCH_ASSOC);
                $Membre = New Membre($memb);//PDO::FETCH_OBJ);

            } 

            return $Membre;
        }
        
        /**
         * Ajoute un membre à la bd
         * 
         * @param Membre $Membre objet représentant la membre à ajouter
         * @return Membre $Membre objet représentant la membre qui a été ajoutée
         * L'objet membre contient un id s'il a bien été ajouté à la bd
         * @throws Exception Si la membre n'a pas pu être créée
         */
        public static function create($Membre) {
            
            $sql = 'INSERT INTO Membre (pseudo, nom, prenom, sexe, sport, courriel, date_inscription, type_membre, mdp ) VALUES (:pseudo, :nom, :prenom, :sexe, :sport, :courriel, :date_inscription, :type_membre, :mdp )';

            try {
                $resultat = self::executerRequete($sql, $Membre->toArray());

                if($resultat->rowCount() > 0) {
                    $id = self::getBdd()->lastInsertId(); 
                    // récupère l'id de la dernière membre créée en cas de succès
                    $Membre->setID($id);

                } else {

                    throw new Exception("La membre n'a pas pu être ajouté à la base de données");
                }

            } catch(Exception $e) {
                throw new Exception("Le membre n'a pas pu être ajouté à la base de donnée");
            }

            return $Membre;

        }

        public static function deleteBytitre($pseudo){
           

            $sql = 'SELECT id from Membre WHERE pseudo = :pseudo';


            try{
                $resultat = self::executerRequete($sql, array('pseudo' => $pseudo));
                
                if($resultat->rowCount() > 0) {

                    $tab = $resultat->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($tab as $value) {
                        $id = $value['id'];

                        $sql = 'UPDATE Equipe SET capitaine = NULL WHERE capitaine = :id';

                        $resultat = self::executerRequete($sql, array('id' => $id));
                    }
                }

            }
            catch (Exception $e){
                throw new Exception ("Nous n avons pas pu extraire le id du capitaine.");
            }

            

            $reussite = false;
             $sql = 'DELETE  FROM Membre WHERE pseudo = :pseudo';
            
             try {

                $resultat = self::executerRequete($sql, array('pseudo' => $pseudo));

                if($resultat->rowCount() > 0) {
                    $reussite = true;

                }else{
                    throw new Exception ("Le membre n'a pas pu être supprimé.");
                }

             } catch(Exception $e){
                throw new Exception ("Une erreur est survenue lors de l'effacement.");
             }

             return $reussite;
        }

        public static function update($Membre){

            // Fabrication de la clause SET
            $reussite = false;
            $clause_set = "";
            $param = $Membre->toArray();
            $cles = array_keys($param);
            $derniere = end($cles);
            if(count($param) > 0) {
                $clause_set = " SET ";
                foreach($param as $cle => $contenu) {
                    if($cle !=='id'){

                        $clause_set = $clause_set . $cle . " = :" . $cle;
                        if ($cle !== $derniere) {
                            $clause_set = $clause_set . ", ";
                        }
                    }

                }
            }

            $sql = 'UPDATE Membre' . $clause_set .' WHERE id = :id';


            try{
                $resultat = self::executerRequete($sql, $Membre->toArray());

                if($resultat->rowCount() >0){
                
                    $id = self::getBdd()->lastInsertId();
                    $Membre->setID($id);
                    $reussite = true;

                }else{
                    throw new Exception("Le membre numéro " . $Membre->getID() . " n\'a pas été mise à jour.");
                

                }
            }catch(Exception $e){
                throw new Exception("Problème survenu lors de la création de la requête.", 1);
                
            }

            return $reussite;
        }
        

    }
