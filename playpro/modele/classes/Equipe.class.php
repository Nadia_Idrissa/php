<?php

class Equipe {
    private $nom_equipe;
    private $capitaine;
    private $sport;
    private $nb_parties_jouees;
    private $nb_joueurs;
    private $nb_max_joueurs;
    private $date_creation;

    
    
    function __construct(array $donnees) {
        $this->hydrate($donnees);
    }
    
    function setNom_equipe($value){
        $this->nom_equipe = $value;
    }
    
    function setCapitaine($value){
        $this->capitaine = $value;
    }

    function setSport($value){
        $this->sport = $value;
    }
    
    function setNb_parties_jouees($value){
        $this->nb_parties_jouees = $value;
    }

    function setNb_joueurs($value){
        $this->nb_joueurs = $value;
    }

    function setNb_max_joueurs($value){
        $this->nb_max_joueurs = $value;
    }
    
    function setDate_creation($value){
        $this->date_creation = $value;
    }
    
    function getNom_equipe(){
        return $this->nom_equipe;
    }
    
    function getCapitaine(){
        return $this->capitaine;
    }

    function getSport(){
        return $this->sport;
    }
    
    function getNb_parties_jouees(){
        return $this->nb_parties_jouees;
    }

    function getNb_max_joueurs(){
        return $this->nb_max_joueurs;
    }

    function getNb_joueurs(){
        return $this->nb_joueurs;
    }

    function getDate_creation(){
        return $this->date_creation;
    }

    public function hydrate(array $donnees){
        foreach ($donnees as $key => $value)
            {
                // On récupère le nom du setter correspondant à l'attribut.
                $method = 'set'.ucfirst($key);
                
                // Si le setter correspondant existe.
                if (method_exists($this, $method))
                {
                    // On appelle le setter.
                    $this->$method($value);
                }
            }
    } 

    public function toArray(){
        $tab = array();

        if(!is_null($this->nom_equipe)){
            $tab['nom_equipe'] = $this->nom_equipe;
        }

        if(!is_null($this->capitaine)){
            $tab['capitaine'] = $this->capitaine;
        }

        if(!is_null($this->sport)){
            $tab['sport'] = $this->sport;
        }

        if(!is_null($this->nb_parties_jouees)){
            $tab['nb_parties_jouees'] = $this->nb_parties_jouees;
        }

        if(!is_null($this->nb_joueurs)){
            $tab['nb_joueurs'] = $this->nb_joueurs;
        }

        if(!is_null($this->nb_max_joueurs)){
            $tab['nb_max_joueurs'] = $this->nb_max_joueurs;
        }

        if(!is_null($this->date_creation)){
            $tab['date_creation'] = $this->date_creation;
        }

        return $tab;

    }
        
       
}