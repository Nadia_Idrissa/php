<?php
require_once('./controleur/Action.interface.php');
require_once('./vues/Page.class.php');

class LogoutControleur implements Action {
	public function execute(){
		if (!ISSET($_SESSION)) 
			session_start();
		UNSET($_SESSION["connected"]);
		session_destroy();
		return new Page("logout", "Mon site - Accueil", null, null);
	}
}
?>