<?php
require_once('./controleur/Action.interface.php');
require_once('./vues/Page.class.php');
require_once('./modele/MembreDAO.class.php');
require_once('./modele/EquipeDAO.class.php');
require_once('./modele/PartieDAO.class.php');
require_once('./classes/Message.class.php');


	class SupprimerequipeControleur implements Action {
	public function execute(){
		if (!ISSET($_REQUEST["nom"]))
			return new Page("accueil", "PlayPro - Accueil", null, null);
		if (!ISSET($_SESSION)) session_start();

		
		$edao = new EquipeDAO();


		$message = "L'équipe ". $_REQUEST['nom']." a bien été supprimée.";
		
		if (!$this->valide())
		{
			$data = $edao->findAll();
			$message = "Cette equipe [" . $_REQUEST['nom'] . "] est associé à des parties. Supprimez les parties en premier.";
			//$_REQUEST["global_message"] = "Le formulaire contient des erreurs. Veuillez les corriger.";	
			return new Page("afficherequipes", "PlayPro - Les équipes", $data, $message);
		}

		$supprime = $edao->DeleteById($_REQUEST['nom']);
		
		if($supprime)

		{
			$data = $edao->findAll();
			return new Page("afficherequipes", "PlayPro -Équipe supprimée", $data, $message);

		}else{
			$message = "L'équipe n'a pas été supprimée.";
			$cree = $edao->create($cree);
			return new Page("creerequipe", "PlayPro - Équipe non supprimé", $cree, null);
		}
		
		
		if ($user == null)
			{
				
				$_REQUEST["field_messages"]["username"] = "Utilisateur inexistant.";	
				return new Page("portail", "PlayPro - Connecté", null, null);
			}

		
		// else if (!password_verify($_REQUEST['password'],$user->getMdp()))
		// 	{
		// 		$_REQUEST["field_messages"]["password"] = "Mot de passe incorrect.";	
		// 		return new Page("accueil", "PlayPro - Mot de passe incorecte", null, null);
		// 	}
		
		// $_SESSION["connected"] = $_REQUEST["username"];
		// echo $user->getPrenom();
		
		// $_SESSION["connected"] = $user->getPrenom();

	}


	public function valide()
	{
		$resultat = true;
		$pdao = new PartieDAO();
		if ($_REQUEST['nom'] == "")
		{
			
			$resultat = false;
		}

		if($pdao->findByEquipe($_REQUEST['nom'])){	
		
			$resultat = false;
		}	
		return $resultat;
	}
}