<?php

	// -- Contrôleur frontal --
	require_once('./controleur/Routeur.class.php');
	date_default_timezone_set('America/New_York');
	// echo "Today is " . date("Y-m-d") . "<br>";
	if (ISSET($_REQUEST["action"]))
		{
			//$vue = ActionBuilder::getAction($_REQUEST["action"])->execute();
			/*
			Ou :*/
			// echo $_REQUEST["action"];
			$actionDemandee = $_REQUEST["action"];
			$controleur = Routeur::getAction($actionDemandee);
			/**/
		}
	else	
		{
			$controleur = Routeur::getAction("accueil");
		}

	$vue = $controleur->execute();

	echo $vue->genererContenu();
?>
