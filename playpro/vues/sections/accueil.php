
<div class="slider">
  <ul class="slides blue">
    <li>
      <img src="vues/sections/images/17334579098_5d88ddb37f_b.jpg" id="nosimages"> <!-- random image -->
      <div class="caption center-align">
        <h3>I</h3>
        <h5 class="light grey-text text-lighten-3">Enfin, un moyen pour vous amuser</h5>
      </div>
    </li>
    <li>
      <img src="vues/sections/images/maxresdefault.jpg" id="nosimages"> <!-- random image -->
      <div class="caption left-align">
        <h3 class="light black-text text-lighten-3">II</h3>
        <h5 class="light black-text text-lighten-3">Une plate-forme facile à utiliser</h5>
      </div>
    </li>
    <li>
      <img src="vues/sections/images/soccer-673474_960_720.jpg" id="nosimages"> <!-- random image -->
      <div class="caption right-align">
        <h3 class="light black-text text-lighten-3">III</h3>
        <h5 class="light black-text text-lighten-3">Vous permet d'explorer votre potentiel</h5>
      </div>
    </li>
    <li>
      <img src="vues/sections/images/soccer-933037_960_720.jpg" id="nosimages"> <!-- random image -->
      <div class="caption center-align">
        <h3 class="light black-text text-lighten-3">IV</h3>
        <h5 class="light black-text text-lighten-3">Tout en étant amateur ...</h5>
      </div>
    </li>
  </ul>
</div>

<section id="search" class="section section-search blue darken-1 white-text center scrollspy">
  <form>
  <div class="container">
      
      <div class="row">
        <div id = "chercher" class="col s12">
          <h3>Chercher une partie</h3>
          <!-- <div  class="col s12 m6 "><h5 class="left">Choisis un sport</h5></div> -->
          <div class="input-field m5  white grey-text" style="margin-right: 150px; margin-left: 150px;">
            <select name ="sport" type="selected" >
                <option value="" disabled selected>Choisissez un sport </option>
                <option value="Soccer">Soccer</option>
                <option value="Hockey">Hockey</option>
                <option value="Badminton">Badminton</option>
                <option value="Tous les sports">Tous les sports</option>
            </select>
          </div>
        </div>

          <div class=" m5 input-field black-text" style="margin-right: 160px; margin-left: 160px;">
            <input type="text" name="date"  class="datepicker red-text white" placeholder="choisissez une date">
          </div>

          <div class=" m5 "  >
            <button type='submit'   title='Chercher' name='action' value='chercherparties' class='large col s7 btn-large waves-effect green modal-trigger' style="cursor: pointer; text-align: center;  font-size: 20px; border-radius: 12px;margin-right: 20%; margin-left: 20%;"><i class="material-icons">search</i>Chercher
            </button>
          </div>
      </div>
  </div>
  </form>
</section>



</br></br></br>



<div class="container">

  <div class="row center">
    <div id="test" class="col s12 m3">
      <i class="material-icons large blue-text center">directions_run</i>
      <h5 id="mini-title">&rArr;</h5>
      <div class="card-panel blue">
        <div class="white-text" >Envie de jouer?</div><hr/>
        <span class="white-text">Un sport collectif? tu ne connais personne disponible?

        </span>

      </div>
    </div>
    <!-- </div> -->


    <div id="test" class="col s12 m3">
      <i class="material-icons large blue-text center">person_pin_circle</i>
      <h5 id="masquex">B</h5>
      <div class="card-panel blue">
        <div class="white-text" >Trouve où ... </div><hr/>
        <span class="white-text"> Fais une recherche rapide et choisis la partie convenable.
        </span>
      </div>
    </div>


    <div id="test" class="col s12 m3">
      <i class="material-icons large blue-text center">thumb_up</i>
      <h5 id="masquex">C</h5>
      <div class="card-panel blue">
        <div class="white-text" >Confirme ...</div><hr/>
        <span class="white-text">envoie une notification
          au responsable pour confirmer ta présence.
        </span>
      </div>
    </div>

    <div id="test" class="col s12 m3">
      <i class="material-icons large blue-text center">tag_faces</i>

      <h5 id="masquex"> D</h5>
      <div class="card-panel blue">
        <div class="white-text" >Amuse-toi !</div><hr/>
        <span class="white-text">Profite du moment avec d'autres amateurs avec professionalisme.
        </span>
      </div>

    </div>

  </div>
</div>







  <!-- Element Showed -->
  <div class="fixed-action-btn">
    <a id="aide" class="waves-effect waves-light btn-large btn-floating orange pulse" ><i class="material-icons">help</i></a>
  </div>


  <!-- Tap Target Structure -->
  <div class="tap-target orange" data-target="aide">
    <div class="tap-target-content">
      <h5>&nbsp;&nbsp;&nbsp;  Petite astuce</h5>
      <p>La première chose à faire est de vous créer un compte.<br>
      Puis, dans le menu, cliquez sur jouer et choisissez une équipe qui manque de joueurs ou créez la votre!</p>

    </div>
  </div>



