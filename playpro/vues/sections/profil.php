







<div class="container">
  <div class="col s8 m8 offset-m2 l6 offset-l3">
    <div class="card-panel grey lighten-5 z-depth-1">
      <div class="row valign-wrapper">
        <div class="col s2">
          <img src="./vues/sections/images/user.jpg" alt="" class="circle responsive-img">
          <!-- notice the "circle" class -->
        </div>
        <div class="col s6">
          <span class="black-text">

            <div class="input-field col s12">
              <input placeholder="<?php echo $data->getPseudo()?>" id="pseudonyme" name = "pseudonyme" type="text" class="validate" disabled>
              <label for="pseudonyme">Pseudonyme</label>
            </div>

            <div class="input-field col s12">
              <input placeholder="<?php echo $data->getPrenom()?>" id="prenom" name="prenom" type="text" class="validate" disabled>
              <label for="prenom">Prénom</label>
            </div>

            <div class="input-field col s12">
              <input placeholder="<?php echo $data->getNom()?>" id="nom" name="nom" type="text" class="validate" disabled>
              <label for="nom">Nom</label>
            </div>

            <div class="input-field col s12">
              <input placeholder="<?php echo $data->getSexe()?>" id="sexe" name="sexe" type="text" disabled >
              <label for="sexe">Sexe</label>
            </div>

             <?php
              $udao = new MembreDAO();
              $user = $udao->findBytitre($_SESSION['connected']);
              if ($user->getType_membre() ==="admin")

                { ?>
                  <div class="input-field col s12">
                    <input placeholder="<?php echo $data->getType_membre()?>" id="type_membre" name="type_membre" type="text" disabled >
                    <label for="type_membre">Type membre</label>
                  </div>

                  <div class="input-field col s12">
                    <input placeholder="<?php echo $data->getDate_inscription()?>" id="date" name="date" type="text" class="validate" disabled>
                    <label for="date">Inscrit depuis:</label>
                  </div>

                  <div class="input-field col s12">
                    <input placeholder="<?php echo $data->getCourriel()?>" id="courriel" name="courriel" type="text" class="validate" disabled>
                    <label for="courriel">Courriel</label>
                  </div>

                    <div class=" col s12">
                      <form>
                        <input value="<?php echo $data->getPseudo()?>" id="pseudo"     name = "pseudo" type="hidden" >
                          
                        <div class="col s6 left">
                          <button type='submit' name='action' title="Supprimer ce compte" value="supprimerjoueur" class='col s12 btn red  waves-effect waves-light ' >Supprimer compte</button>
                        </div>
                        <div class="col s6 right" >
                          <button type='cancel' title="Désactiver ce compte" name='btn_cancel' class='col s12 btn waves-effect orange'>Désactiver compte</button>
                        </div>
                      </form>
                    </div>

                <?php } ?>

          

            
          </span>
        </div>
      </div>
    </div>
  </div>
</div>