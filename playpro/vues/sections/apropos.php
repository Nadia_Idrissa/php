


<div class="container left">

 <div class="col s4 m6 offset-m2 l6 offset-l3">
  <div class="card-panel grey lighten-5 z-depth-1">
    <div class="row valign-wrapper">
      <div class="col s2">
        <img id = "img2" src="vues/sections/images/head01.png" alt="" class="circle responsive-img"> <!-- notice the "circle" class -->
      </div>
      <div class="col s6 right">
        <span class="black-text">
          Je m'appelle Younes DILALI. Cette plate-forme est le fruit d'une longue réflexion.
          Cette idée originale a vu le jour suite à un simple besoin. Ceci est relatif à l'exercice d'une activité collective sans la nécessité d'avoir la disponibilité de toutes tes connaissances.
          Via ce site, vous pouvez créer des parties ou en faire partie. Avec PlayPro, pratiquez votre sport préféré en tant qu'amateur avec professionalisme. 
          <a href="vues/sections/cvdilali/index.php">Pour en savoir plus sur moi!</a>
        </span>
      </div>
    </div>
  </div>
</div>

</div>

<div class="container right">

 <div class="col s4 m6 offset-m2 l6 offset-l3">
  <div class="card-panel grey lighten-5 z-depth-1">
    <div class="row valign-wrapper">

      <div class="col s6 right">
        <span class="black-text">
          Mon nom est Kennedy KALOMBA. J'ai eu le privilège de travailler avec le créateur de cette plate-forme, Younes.
          J'ai trouvé son idée géniale et je n'ai pas hésité une seule seconde avant d'accepter de travailler avec lui.
          J'ai donc fait le choix d'être son collaborateur maintenant plutôt que son employé dans un futur proche.
          Avec PlayPro, pratiquez votre sport préféré en tant qu'amateur avec professionalisme.
          <a href="vues/sections/cvkalomba/cvken.php">Voir mon curriculum vitae.</a>
        </span>
      </div>

      <div class="col s2">
        <img id = "img2" src="vues/sections/cvkalomba/images/136.jpg" alt="" class="circle responsive-img"> <!-- notice the "circle" class -->
      </div>
    </div>
  </div>
</div>

</div>





<!--JavaScript at end of body for optimized loading-->
<script type="text/javascript" src="js/materialize.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">

<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

<script>
  $(document).ready(function(){
    $('.sidenav').sidenav();
    $('.slider').slider();
    $('select').formSelect();
    $('.modal').modal();
  }); 

  $('.dropdown-trigger').dropdown();

  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.fixed-action-btn');
    var instances = M.FloatingActionButton.init(elems, {
      direction: 'bottom',
      hoverEnabled: false
    });
  });

</script>


