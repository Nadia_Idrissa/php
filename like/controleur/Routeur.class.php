<?php
require_once('./controleur/AccueilControleur.class.php');
// require_once('./controleur/AproposControleur.class.php');
// require_once('./controleur/ContactControleur.class.php');
// require_once('./controleur/PortailControleur.class.php');
require_once('./controleur/ConnexionControleur.class.php');
require_once('./controleur/InscriptionControleur.class.php');
// require_once('./controleur/ErreurControleur.class.php');
// require_once('./controleur/SupprimerControleur.class.php');
// require_once('./controleur/CreerControleur.class.php');
// require_once('./controleur/ChercherControleur.class.php');
// require_once('./controleur/ModifierControleur.class.php');
// require_once('./controleur/QuoiControleur.class.php');
// require_once('./controleur/CommentControleur.class.php');
// require_once('./controleur/ProfilControleur.class.php');
// require_once('./controleur/AfficherControleur.class.php');



class Routeur{
	public static function getAction($nomAction){
		
		$classe = ucfirst($nomAction) . 'Controleur';
		
				
		if (class_exists($classe)) {
			
			return new $classe();
		} else {
			return new ErreurControleur();
		}

	}
}
?>
