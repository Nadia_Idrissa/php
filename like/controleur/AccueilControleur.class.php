<?php
require_once('./controleur/Action.interface.php');
require_once('./vues/Page.class.php');

class AccueilControleur implements Action {
	public function execute(){
		//vue / nom de la page - onglet, object object
		return new Page("accueil", "PlayPro - Accueil", null, null);
	}
}
?>