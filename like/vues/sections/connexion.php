<!DOCTYPE html>
<html lang="en">
   <head>
        <title>Bootstrap 3 simple login form free template</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
   </head>
<body>
    <div class="login">
            <div class="account-login">
               <h1>Connexion</h1>
               <form action="" class="login-form">
                  <div class="form-group">
                     <input name="courriel" type="text" placeholder="User Name" class="form-control">
                  </div>
                  <div class="form-group">
                     <input name="mdp" type="password" placeholder="Password"  class="form-control">
                  </div>                  
                  <button class="btn" type="submit">Login</button>
                  <p>Are you new?<a href="?action=inscription">Sign Up</a></p>
               </form>
            </div>
        </div>
   </body>
</html>
