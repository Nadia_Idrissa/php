<!DOCTYPE html>
<html>
<head>

	<meta charset="utf-8">

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>
	<!--Let browser know website is optimized for mobile-->
	<link rel="stylesheet" type="text/css" href="./vues/css/styles.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title><?php echo $titre; ?></title>
</head>


<body>
	<div>
		<?php 
		if (!ISSET($_SESSION)) {
			session_start();
		}
		if (ISSET($_SESSION["connected"])){
			
			include("headers/headerjoueur.php");
		}

		else {
			include("headers/header.php");
		}
		

		?>
	</div>
	<main id="content">
		<?php 
		
		echo $contenu; ?>
	</main>
	<div>
		<?php
		include("footer.php");
		?>
	</div>
	
</body>

	

