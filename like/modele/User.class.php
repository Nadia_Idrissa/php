<?php

class User {
    private $courriel;
    private $mdp;
    
    function __construct(array $donnees) {
        $this->hydrate($donnees);
    }
        
    function setCourriel($value){
        $this->courriel = $value;
    }
    function setMdp($value){
        $this->mdp = $value;
    }

    
    function getCourriel(){
        return $this->courriel;
    }
       
    
    function getMdp(){
        return $this->mdp;
    }

   
    public function hydrate(array $donnees){
        foreach ($donnees as $key => $value)
           
		  	{
		    	// On récupère le nom du setter correspondant à l'attribut.
		    	$method = 'set'.ucfirst($key);
               	        
		    	// Si le setter correspondant existe.
		    	if (method_exists($this, $method))
		    	{
		      		// On appelle le setter.
		      		$this->$method($value);
		    	}
		  	}

    } 

    public function toArray(){
        $tab = array();

        if(!is_null($this->courriel)){
            $tab['courriel'] = $this->courriel;
        }
        
        if(!is_null($this->mdp)){
            $tab['mdp'] = $this->mdp;
        }

        return $tab;

    }        
    
}